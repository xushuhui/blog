---
title: 基金初级训练营（Day 1）
date: 2018-08-06 12:15:12
tags: 基金
categories: 生活
---
## 基金课程

#### 1.认识不同的基金
#### 2.挑选种类基金
#### 3.最适合新手的指数基金及投资方法
#### 4.基金投资心理建设

## 基金定义

#### 1.基金是基金公司按证监会规定收集投资者资金进行投资的一种投资品
#### 2.投资人负责出钱
#### 3.基金公司负责根据市场行情进行投资
#### 4.银行负责保管投资人资金

## 基金优势

#### 1.投资金额少，门槛低，手续费低
#### 2.有效分散风险
#### 3.专业基金经理打理，省时省心省力

## 基金风险

#### 1.主要影响因素基金投资的产品种类，股票占比越高风险越高
#### 2.基金资金量大小
#### 3.基金经理能力
#### 3.选择基金匹配自身情况